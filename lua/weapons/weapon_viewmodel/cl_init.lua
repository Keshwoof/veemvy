include 'shared.lua'

--[[ LOCALIZATION ]]
language.Add( 'weapon_viewmodel', 'View Model' )

--[[ NETWORKING ]]
net.Receive( 'ViewModel.PrimaryAttack', function( length, sender )

	local weapon = net.ReadEntity()

	if IsValid( weapon ) and weapon:IsScripted() then
		weapon:PrimaryAttack()
	end

end )

net.Receive( 'ViewModel.SecondaryAttack', function( length, sender )

	local weapon = net.ReadEntity()

	if IsValid( weapon ) and weapon:IsScripted() then
		weapon:SecondaryAttack()
	end

end )

net.Receive( 'ViewModel.Reload', function( length, sender )

	local weapon = net.ReadEntity()

	if IsValid( weapon ) and weapon:IsScripted() then
		weapon:Reload()
	end

end )

--[[ CONVENIENCE FUNCTIONS ]]
local ScrW, ScrH = surface.ScreenWidth(), surface.ScreenHeight()

local function ScaleW(size)
	return ScrW * size / 1920
end

local function ScaleH(size)
	return ScrH * size / 1080
end

--[[ "VIEWMODEL" FUNCTIONS ]]
function SWEP:DrawWorldModel( flags )

end

function SWEP:DrawWorldModelTranslucent( flags )

end

SWEP.ViewModelLocks = {}

for viewModelIndex = 0, 2 do

	SWEP.ViewModelLocks[ viewModelIndex ] = {
		Pos = Vector( 0, 0, 0);
		Ang = Angle( 0, 0, 0);
	};

end

function SWEP:CalcViewModelOffset( origin, angles, offsetPos, offsetAngles)

    angles = angles * 1
	angles:RotateAroundAxis(angles:Right(), offsetAngles.p)
	angles:RotateAroundAxis(angles:Up(), offsetAngles.y)
	angles:RotateAroundAxis(angles:Forward(), offsetAngles.r)

	origin = origin + offsetPos.x * angles:Right()
	origin = origin + offsetPos.y * angles:Forward()
	origin = origin - offsetPos.z * angles:Up()

	return origin, angles
end

local viewmodel_origin_x = CreateConVar( 'viewmodel_origin_x', '0', bit.bor( FCVAR_USERINFO ), 'Set viewmodel $origin (x-axis)' )
local viewmodel_origin_y = CreateConVar( 'viewmodel_origin_y', '0', bit.bor( FCVAR_USERINFO ), 'Set viewmodel $origin (y-axis)' )
local viewmodel_origin_z = CreateConVar( 'viewmodel_origin_z', '0', bit.bor( FCVAR_USERINFO ), 'Set viewmodel $origin (z-axis)' )
local viewmodel_origin_scalar = CreateConVar( 'viewmodel_origin_scalar', '5', bit.bor( FCVAR_USERINFO ), 'Set viewmodel $origin offset scalar' )
local viewmodel_origin_z_rotation = CreateConVar( 'viewmodel_origin_z_rotation', '0', bit.bor( FCVAR_USERINFO ), 'Set viewmodel $origin (z-axis, rotation)' )
local viewmodel_origin_z_rotation_max = CreateConVar( 'viewmodel_origin_z_rotation_max', '45', bit.bor( FCVAR_USERINFO ), 'Set maximum viewmodel $origin rotation angle' )

concommand.Add( 'viewmodel_reset_origin', function(player, command, arguments)

	viewmodel_origin_x:SetFloat( viewmodel_origin_x:GetDefault() )
	viewmodel_origin_y:SetFloat( viewmodel_origin_y:GetDefault() )
	viewmodel_origin_z:SetFloat( viewmodel_origin_z:GetDefault() )
	viewmodel_origin_z_rotation:SetFloat( viewmodel_origin_z_rotation:GetDefault() )

	viewmodel_origin_scalar:SetFloat( viewmodel_origin_scalar:GetDefault() )
	viewmodel_origin_z_rotation_max:SetFloat( viewmodel_origin_z_rotation_max:GetDefault() )

end, nil, 'Reset $origin for Veemvy' )

local viewmodel_lock = CreateConVar( 'viewmodel_lock', '0', bit.bor( FCVAR_USERINFO ), 'Lock viewmodel position/angles in preview?' )

local offsetPos, offsetAngles = Vector( 0, 0, 0), Angle( 0, 0, 0)

function SWEP:CalcViewModelView( viewModel, eyePos, eyeAngles, viewModelPos, viewModelAngles )

	offsetPos.x = viewmodel_origin_x:GetFloat()
	offsetPos.y = viewmodel_origin_y:GetFloat()
	offsetPos.z = viewmodel_origin_z:GetFloat() * viewmodel_origin_scalar:GetFloat()

	offsetAngles.y = viewmodel_origin_z_rotation:GetFloat()

	eyePos, eyeAngles = self:CalcViewModelOffset( eyePos, eyeAngles, offsetPos, offsetAngles )

	local viewModelIndex = viewModel:ViewModelIndex()
	local viewModelLock = self.ViewModelLocks[ viewModelIndex ]

	if viewmodel_lock:GetBool() then

		if viewModelLock.Pos:IsZero() then
			viewModelLock.Pos:Set( eyePos )
		else
			eyePos:Set( viewModelLock.Pos )
		end

		if viewModelLock.Ang:IsZero() then
			viewModelLock.Ang:Set( eyeAngles )
		else
			eyeAngles:Set( viewModelLock.Ang )
		end

	else

		if not viewModelLock.Pos:IsZero() then
			viewModelLock.Pos:Zero()
		end

		if not viewModelLock.Ang:IsZero() then
			viewModelLock.Ang:Zero()
		end

	end

	return eyePos, eyeAngles
end

function SWEP:ShowFileBrowser()

	if self.m_FileBrowser then
		self.m_FileBrowser:Remove()
	end

	local wide, tall = ScaleW( 640.0 ), ScaleH( 480.0 )

	self.m_FileBrowser = vgui.Create( 'DFrame' )
	self.m_FileBrowser:SetTitle( '#viewmodel_browser' )

	self.m_FileBrowser:SetSize( wide, tall )
	self.m_FileBrowser:SetSizable( true )

	self.m_FileBrowser:Center()
	self.m_FileBrowser:MakePopup()

	local DFileBrowser = vgui.Create( 'DFileBrowser', self.m_FileBrowser )
	DFileBrowser:Dock( FILL )

	DFileBrowser:SetPath( 'GAME' )

	DFileBrowser:SetBaseFolder( 'models' )
	DFileBrowser:SetFileTypes( '*.mdl' )

	DFileBrowser:SetOpen( true )
	DFileBrowser:SetCurrentFolder( 'weapons' )

	function DFileBrowser:OnSelect( path, this )
		RunConsoleCommand( 'viewmodel_path', path )
	end

	function DFileBrowser:OnDoubleClick( path, this )
		self.m_FileBrowser:Close()
	end

end

function SWEP:ShowViewModelViewer()

	if self.m_ViewModelViewer then
		self.m_ViewModelViewer:Remove()
	end

	local wide, tall = ScaleW( 640.0 ), ScaleH( math.floor( 1080 * 0.8 ) )

	self.m_ViewModelViewer = vgui.Create( 'DFrame' )
	self.m_ViewModelViewer:SetTitle( '#veemvy_title' )

	self.m_ViewModelViewer:SetSize( wide, tall )

--	self.m_ViewModelViewer:SetPos( 0, 0) -- stick to left side
	self.m_ViewModelViewer:Center()

	self.m_ViewModelViewer:MakePopup()

	self.m_ViewModelViewer:SetKeyboardInputEnabled( false )

	local DScrollPanel = vgui.Create( 'DScrollPanel', self.m_ViewModelViewer )
	DScrollPanel:Dock( FILL )

	local DCollapsibleCategory = DScrollPanel:Add( 'DCollapsibleCategory' )
	DCollapsibleCategory:SetLabel( '#viewmodel_viewer' )
	DCollapsibleCategory:SetSize( wide, tall )
	DCollapsibleCategory:SetExpanded( true )

	DCollapsibleCategory:Dock( TOP )

	local DPanelList = DScrollPanel:Add( 'DPanelList' )
	DPanelList:SetSpacing( 5 )
	DPanelList:EnableHorizontal( false )
	DPanelList:EnableVerticalScrollbar( true )
	DCollapsibleCategory:SetContents( DPanelList )

	local DButton = vgui.Create( 'DButton' )
	DButton:SetText( '#viewmodel_reset' )
	DButton:Dock( TOP )

	DButton.DoClick = function( this )
		RunConsoleCommand( 'viewmodel_reset' )
		RunConsoleCommand( 'viewmodel_lock', 0 )
	end

	DPanelList:AddItem( DButton )

	local DButton = vgui.Create( 'DButton' )
	DButton:SetText( '#viewmodel_browser' )
	DButton:Dock( TOP )

	DButton.DoClick = function( this )
		self:ShowFileBrowser()
	end

	DPanelList:AddItem( DButton )

	local DNumSlider = vgui.Create( 'DNumSlider' )
	DNumSlider:SetText( '#viewmodel_index' )
	DNumSlider:SetConVar( 'viewmodel_index' )
	DNumSlider:SetMin( 0 )
	DNumSlider:SetMax( 2 )
	DNumSlider:SetDecimals( 0 )
	DNumSlider:Dock( TOP )

	DPanelList:AddItem( DNumSlider )

	local fov_desired = GetConVar( 'fov_desired' )

	local DNumSlider = vgui.Create( 'DNumSlider' )
	DNumSlider:SetText( '#fov_viewmodel' )
	DNumSlider:SetConVar( 'fov_viewmodel' )
	DNumSlider:SetMin( fov_desired:GetMin() )
	DNumSlider:SetMax( fov_desired:GetMax() )
	DNumSlider:SetDecimals( 0 )
	DNumSlider:Dock( TOP )

	DPanelList:AddItem( DNumSlider )

	local DNumSlider = vgui.Create( 'DNumSlider' )
	DNumSlider:SetText( '#viewmodel_fov' )
	DNumSlider:SetConVar( 'viewmodel_fov' )
	DNumSlider:SetMin( 0.1 )
	DNumSlider:SetMax( 179 )
	DNumSlider:SetDecimals( 1 )
	DNumSlider:Dock( TOP )

	DPanelList:AddItem( DNumSlider )

	local DCheckBoxLabel = vgui.Create( 'DCheckBoxLabel' )
	DCheckBoxLabel:SetText( '#viewmodel_flip' )
	DCheckBoxLabel:SetConVar( 'viewmodel_flip' )
	DCheckBoxLabel:Dock( TOP )

	DPanelList:AddItem( DCheckBoxLabel )

	local DCheckBoxLabel = vgui.Create( 'DCheckBoxLabel' )
	DCheckBoxLabel:SetText( '#viewmodel_idle' )
	DCheckBoxLabel:SetConVar( 'viewmodel_idle' )
	DCheckBoxLabel:Dock( TOP )

	DPanelList:AddItem( DCheckBoxLabel )

	local DCheckBoxLabel = DScrollPanel:Add( 'DCheckBoxLabel' )
	DCheckBoxLabel:SetText( '#viewmodel_lock' )
	DCheckBoxLabel:SetConVar( 'viewmodel_lock' )
	DCheckBoxLabel:Dock( TOP )

	DPanelList:AddItem( DCheckBoxLabel )

	local DComboBox = DScrollPanel:Add( 'DComboBox' )

	local viewModelIndex = 0
	local viewModel = self.Owner:GetViewModel( viewModelIndex )

	for index, sequenceName in ipairs( viewModel:GetSequenceList() ) do

		DComboBox:AddChoice( sequenceName )

		DComboBox.OnSelect = function( this, index, value )

			net.Start( 'ViewModel.Sequence' )
				net.WriteString( value )
			net.SendToServer()

		end

	end

	DPanelList:AddItem( DComboBox )

	local DCollapsibleCategory = DScrollPanel:Add( 'DCollapsibleCategory' )
	DCollapsibleCategory:SetLabel( '#veemvy_qctools' )
	DCollapsibleCategory:SetSize( wide, tall )
	DCollapsibleCategory:SetExpanded( true )

	DCollapsibleCategory:Dock( TOP )

	local DPanelList = DScrollPanel:Add( 'DPanelList' )
	DPanelList:SetSpacing( 5 )
	DPanelList:EnableHorizontal( false )
	DPanelList:EnableVerticalScrollbar( true )
	DCollapsibleCategory:SetContents( DPanelList )

	local DButton = vgui.Create( 'DButton' )
	DButton:SetText( '#viewmodel_reset_origin' )
	DButton:Dock( TOP )

	DButton.DoClick = function( this )
		RunConsoleCommand( 'viewmodel_reset_origin' )
	end

	DPanelList:AddItem( DButton )

	local size = ScaleH( 240.0 )

	local DSliderBuffer = DScrollPanel:Add( 'Panel' )
	DSliderBuffer:SetSize( size, size )
	DSliderBuffer:Dock( TOP )
	DSliderBuffer:DockMargin( wide / 2 - size / 2, ScaleW( 10 ), 0, ScaleH( 10 ) )
	DSliderBuffer:Center()

	DPanelList:AddItem( DSliderBuffer )

	local DSlider = DSliderBuffer:Add( 'DSlider' )
	DSlider:SetText( '#viewmodel_origin_xy' )
	DSlider:SetLockX()
	DSlider:SetLockY()

	DSlider:SetSize( size, size )
	DSlider:Center()

	DSlider.TranslateValues = function( this, x, y)

		local newX, newY = (2 * x) - 1, (2 * y) - 1
		local scalar = viewmodel_origin_scalar:GetFloat()

		viewmodel_origin_x:SetFloat( newX * scalar )
		viewmodel_origin_y:SetFloat( newY *-scalar )

		return x, y
	end

	local DSliderAlpha = math.floor( 255 * 0.4 )
	local DSliderSegments = 20

	DSlider.Paint = function( this, wide, tall)

		surface.SetDrawColor( 255, 255, 255, DSliderAlpha )
	    surface.DrawRect( 0, 0, wide, tall)

		local xSegments = wide / DSliderSegments
		local ySegments = tall / DSliderSegments

		surface.SetDrawColor( 0, 0, 0, 255 )

		for xSegment = 1, DSliderSegments - 1, 1 do

			local startX = xSegment * xSegments
			surface.DrawLine( startX, 0, startX, tall )

		end

		for ySegment = 1, DSliderSegments - 1, 1 do

			local startY = ySegment * ySegments
			surface.DrawLine( 0, startY, wide, startY )

		end

	end

	local DNumSlider = vgui.Create( 'DNumSlider', DScrollPanel )
	DNumSlider:SetText( '#viewmodel_origin_z' )
	DNumSlider:SetConVar( 'viewmodel_origin_z' )
	DNumSlider:SetMin( -1 )
	DNumSlider:SetMax(  1 )
	DNumSlider:SetDecimals( 2 )
	DNumSlider:Dock( TOP )

	DPanelList:AddItem( DNumSlider )

	local DNumSliderRange = viewmodel_origin_z_rotation_max:GetFloat()

	local DNumSlider = vgui.Create( 'DNumSlider', DScrollPanel )
	DNumSlider:SetText( '#viewmodel_origin_z_rotation' )
	DNumSlider:SetConVar( 'viewmodel_origin_z_rotation' )
	DNumSlider:SetMin( -DNumSliderRange )
	DNumSlider:SetMax(  DNumSliderRange )
	DNumSlider:SetDecimals( 0 )
	DNumSlider:Dock( TOP )

	DPanelList:AddItem( DNumSlider )

	local DButton = vgui.Create( 'DButton', DScrollPanel )
	DButton:SetText( '#viewmodel_origin_clipboard' )
	DButton:Dock( TOP )
	DButton:DockMargin( 0, 0, 3, 3 )

	DButton.DoClick = function( this )
		local clipboardText = string.format( '$origin %.2f %.2f %.2f %.2f', offsetPos.x, offsetPos.y, offsetPos.z, offsetAngles.y)
		SetClipboardText(clipboardText)
	end

	DPanelList:AddItem( DButton )

end
