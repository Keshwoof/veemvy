SWEP.Base = 'weapon_viewmodel'
DEFINE_BASECLASS( SWEP.Base )

-- [[ VIEWMODEL CONVARS/COMMANDS ]]
local viewmodel_path = CreateConVar( 'viewmodel_path', 'models/weapons/v_rif_ak47.mdl', bit.bor( FCVAR_ARCHIVE, FCVAR_USERINFO), 'Path for viewmodel preview' )
local viewmodel_index = CreateConVar( 'viewmodel_index', '0', bit.bor( FCVAR_ARCHIVE, FCVAR_USERINFO), 'Index for viewmodel preview', 0, 2 )
local viewmodel_flip = CreateConVar( 'viewmodel_flip', '0', bit.bor( FCVAR_USERINFO ), 'Flip viewmodels in preview?' )
local viewmodel_idle = CreateConVar( 'viewmodel_idle', '1', bit.bor( FCVAR_USERINFO ), 'Play idle animations for viewmodel viewer?' )

local viewmodel_fov = GetConVar( 'viewmodel_fov' )

local fov_desired = GetConVar( 'fov_desired' )
local fov_viewmodel = CreateConVar( 'fov_viewmodel', '90', bit.bor( FCVAR_ARCHIVE, FCVAR_USERINFO), 'Player FoV for viewmodel viewer.', fov_desired:GetMin(), fov_desired:GetMax() )

concommand.Add( 'viewmodel_reset', function(player, command, arguments)

	viewmodel_path:SetString( viewmodel_path:GetDefault() )
	viewmodel_index:SetInt( viewmodel_index:GetDefault() )
	viewmodel_flip:SetInt( viewmodel_flip:GetDefault() )
	viewmodel_idle:SetInt( viewmodel_idle:GetDefault() )

--	NOTE: reset to desired value, NOT its default
	fov_viewmodel:SetFloat( fov_desired:GetFloat() )

--	viewmodel_fov:SetFloat( viewmodel_fov:GetDefault() )
	RunConsoleCommand( viewmodel_fov:GetName(), viewmodel_fov:GetDefault() )

end, nil, 'Reset Veemvy' )

concommand.Add( 'viewmodel_viewer', function(player, command, arguments)
	player:Give( 'weapon_viewmodel' )
end, nil, 'Start Veemvy' )

-- [[ VIEWMODEL STRUCTURE ]]

SWEP.Slot					= 0
SWEP.SlotPos				= 10
SWEP.Weight					= 10

SWEP.PrintName				= '#Weapon_ViewModel'
SWEP.Category				= 'Developer'
SWEP.Spawnable				= true
SWEP.AdminOnly				= true

SWEP.ViewModel				= viewmodel_path:GetDefault()
SWEP.WorldModel				= ''
SWEP.HoldType				= 'magic'

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Ammo			= 'none'
SWEP.Primary.Automatic		= false

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Ammo			= 'none'
SWEP.Secondary.Automatic	= false

function SWEP:SetupDataTables()

	self:DTVar( 'Bool', 0, 'Deploy' )

	self:DTVar( 'Float', 0, 'NextIdle' )

end

function SWEP:TranslateFOV( fov )

	local owner = self:GetOwner()

	if IsValid( owner ) then

		fov = owner:GetInfoNum( fov_viewmodel:GetName(), fov )

	end

	return fov
end

local function SendViewModelSequence( owner, sequence )

	local viewModelIndex = 0
	local viewModel = owner:GetViewModel( viewModelIndex )

	if IsValid( viewModel ) then

		local sequenceID

		if isnumber( sequence ) then -- ACT_VM_
			sequenceID = viewModel:SelectWeightedSequence( sequence )
		elseif isstring( sequence ) then
			sequenceID = viewModel:LookupSequence( sequence )
		end

		if isnumber( sequenceID ) then

			viewModel:SendViewModelMatchingSequence( sequenceID )

			return viewModel:SequenceDuration( sequenceID )
		end

	end

end

-- some hackfuckshit for pcall
function SWEP:SendViewModelSequence( sender, sequenceName )
	self.dt.NextIdle = CurTime() + SendViewModelSequence( sender, sequenceName )
end

function SWEP:Initialize( bForce )

	self.dt.Deploy = false

	local owner = self:GetOwner()

	if IsValid( owner ) then

		local viewModelPath = viewmodel_path:GetDefault()
		viewModelPath = owner:GetInfo( viewmodel_path:GetName(), viewModelPath )

		self.ViewModel = viewModelPath

	end

end

function SWEP:Deploy( bForce )

	self.dt.Deploy = true
	self.dt.NextIdle = CurTime() + SendViewModelSequence( self:GetOwner(), ACT_VM_DRAW )

end

function SWEP:PrimaryAttack()

	if game.SinglePlayer() and SERVER then

		net.Start( 'ViewModel.PrimaryAttack' )
			net.WriteEntity( self )
		net.Broadcast()

	end

end

function SWEP:SecondaryAttack()

	if game.SinglePlayer() and SERVER then

		net.Start( 'ViewModel.SecondaryAttack' )
			net.WriteEntity( self )
		net.Broadcast()

	end

	if CLIENT then
		self:ShowViewModelViewer()
	end

end

function SWEP:Reload()

	if game.SinglePlayer() and SERVER then

		net.Start( 'ViewModel.Reload' )
			net.WriteEntity( self )
		net.Broadcast()

	end

	if CLIENT then
		self:ShowFileBrowser()
	end

end

function SWEP:Think()

	if self.dt.Deploy then
		self:Deploy( true )
		self.dt.Deploy = false
	end

	if self:FileSystemThink() then
		return true
	end

	if self:ViewModelThink() then
		return true
	end

	if self:IdleThink() then
		return true
	end

	self:NextThink( CurTime() )
	return true
end

SWEP.Root = 'models/weapons'
SWEP.Path = 'GAME'

SWEP.NextFileSystemCheck = 0

function SWEP:FileSystemThink()

	if CurTime() < self.NextFileSystemCheck then
		return false
	end

--	check folder every x second
	self.NextFileSystemCheck = CurTime() + 10

	local files, folders = file.Find( self.Root..'/v_*.mdl', self.Path, 'datedesc' )

	local fileLast = 0
	local fileLoad

	for index, fileName in ipairs( files ) do

		local fileTime = file.Time( self.Root..'/'..fileName, self.Path )

		if fileTime > fileLast then
			fileLast = fileTime
			fileLoad = fileName
		end

	end

--	load "fresh" .mdl created within a minute
	if ( os.time() - fileLast ) / 60 < 1 then

		viewmodel_path:SetString( self.Root..'/'..fileLoad )
--		print( fileName, os.date('%Y-%m-%d@%H:%M:%S', fileTime ) )

	end

end

function SWEP:ViewModelThink()

	local owner = self:GetOwner()

	local viewModel = NULL
	local viewModelIndex = viewmodel_index:GetInt()

	local viewModelPath = viewmodel_path:GetDefault()
	local viewModelFOV = viewmodel_fov:GetDefault()
	local viewModelFlip = tonumber( viewmodel_flip:GetDefault() )

	if IsValid( owner ) then

		viewModel = owner:GetViewModel( viewModelIndex )

		viewModelPath = owner:GetInfo( viewmodel_path:GetName(), viewModelPath )
		viewModelFOV = owner:GetInfoNum( viewmodel_fov:GetName(), viewModelFOV )

		viewModelFlip = owner:GetInfoNum( viewmodel_flip:GetName(), viewModelFlip )
		viewModelFlip = ( viewModelFlip > 0 ) -- boolean

		if IsValid( viewModel ) then

			if self.ViewModel ~= viewModelPath then

				viewModel:SetWeaponModel( viewModelPath, self )

				self.ViewModel = viewModelPath
				self.dt.Deploy = true

			end

--			the powers that be update this automatically
			if self.ViewModelFOV ~= viewModelFOV then
				self.ViewModelFOV = viewModelFOV
			end

--			ditto
			if self.ViewModelFlip ~= viewModelFlip then
				self.ViewModelFlip = viewModelFlip
			end

		end

	end

end

function SWEP:IdleThink()

	if ( not viewmodel_idle:GetBool() ) then
		return false
	end

	if CurTime() > self.dt.NextIdle then
		self.dt.NextIdle = CurTime() + SendViewModelSequence( self:GetOwner(), ACT_VM_IDLE )
	end

end
