# Veemvy - View Model Viewer
View Model Viewer (VMV) is a developer tool for inspecting view models in Garry's Mod.

## Using Veemvy
Using Veemvy is fairly simple. To start, type `viewmodel_viewer` in-game.<br>
+reload will open the view model file browser. It will find all `v_*.mdl` files in `models/weapons`.<br>
+attack2 will open the main window for inspecting your view model.<br>