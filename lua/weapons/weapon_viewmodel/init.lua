AddCSLuaFile 'cl_init.lua'
AddCSLuaFile 'shared.lua'
include 'shared.lua'

util.AddNetworkString( 'ViewModel.PrimaryAttack' )
util.AddNetworkString( 'ViewModel.SecondaryAttack' )
util.AddNetworkString( 'ViewModel.Reload' )
util.AddNetworkString( 'ViewModel.Sequence' )

net.Receive( 'ViewModel.Sequence', function( length, sender )

	local sequenceName = net.ReadString()

	if string.len( sequenceName ) > 0 then

		if IsValid( sender ) and sender:IsPlayer() then

			local weapon = sender:GetActiveWeapon()

			if IsValid( weapon ) and weapon:IsScripted() then

				pcall( weapon.SendViewModelSequence, weapon, sender, sequenceName )

			end

		end

	end

end )

function SWEP:OnDrop()

	self:Remove()

end
